var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var hobbySchema = new Schema({
    hobby: String,
    phone: String
})

module.exports = mongoose.model("Hobbies", hobbySchema)
