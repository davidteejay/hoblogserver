var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Hobbies = require('../models/hobbies');
var Users = require('../models/users');

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

router.get('/', (req, res) => {
    Hobbies.find((err, data) => {
        if (err) res.send (err);
        res.json(data);
    })
});

router.post('/add', (req, res) => {
    new Hobbies({
        hobby: req.body.hobby,
        phone: req.body.phone
    }).save((err, data) => {
        if (err) res.send(err)
        else {
            let accountSid = 'AC03df9a2816dadc9726374746b8222ce0';
		    let authToken = '034bd371b4531867e6846be1535240ad';

            let twilio = require('twilio');
            let client = new twilio(accountSid, authToken);

            client.messages.create({
                body: 'You just added a hobby - ' + req.body.hobby,
                to: req.body.phone,
                from: '+16106150576'
            })
            .then((message) => {
                console.log("Message Sent")
                res.send("Done")
            })
            .catch(err => res.send(err))
            .done()
        }
    })
})

router.post('/register', (req, res) => {
    new Users({
        email: req.body.email,
        phone: req.body.phone,
        password: req.body.password
    }).save()
    .then(() => res.send("Done"))
    .catch((err) => res.semd(err))
})

router.post('/login', (req, res) => {
    Users.find({ phone: req.body.phone, password: req.body.password }, (err, data) => {
        if (err) res.send(err);
        else if (data.length === 0) res.send("Incorrect")
        else {
            Hobbies.find({ phone: req.body.phone }, (error, datum) => {
                if (error) res.send(error);
                else res.send(datum);
            })
        }
    })
})

module.exports = router;
